<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;

class DeleteTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Transaction::where('transactions_status', 'waiting payment')->delete();
        $this->info("Successfully Delete Waiting Payment Transactions");
    }
}
