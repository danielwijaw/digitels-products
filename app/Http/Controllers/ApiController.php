<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customer;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ResponseFormatter;

use Illuminate\Support\Facades\Auth;

use Config;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->only('name', 'username', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'username' => 'required|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        $user = User::create([
        	'name' => $request->name,
        	'username' => $request->username,
        	'password' => bcrypt($request->password)
        ]);

        return ResponseFormatter::success($user, 'User created successfully');
    }

    public function registerCustomer(Request $request)
    {
        $data = $request->only('name', 'username', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'username' => 'required|unique:customers',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        $user = Customer::create([
        	'name' => $request->name,
        	'username' => $request->username,
        	'password' => bcrypt($request->password)
        ]);

        return ResponseFormatter::success($user, 'User created successfully');
    }
 
    public function authenticate(Request $request)
    {

        $credentials = $request->only('username', 'password');

        $validator = Validator::make($credentials, [
            'username' => 'required',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        if(!$token = Auth::guard('users')->attempt($credentials)){
            return ResponseFormatter::error(null, 'Login credentials are invalid.', 400);
        }

        return ResponseFormatter::success(['token' => $token], 'Login Success');
    }
 
    public function authenticateCustomer(Request $request)
    {

        $credentials = $request->only('username', 'password');

        $validator = Validator::make($credentials, [
            'username' => 'required',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        if (!$token = Auth::guard('customers')->attempt($credentials)) {
            return ResponseFormatter::error(null, 'Login credentials are invalid.', 400);
        }

        return ResponseFormatter::success(['token' => $token], 'Login Success');
    }
 
    public function logout(Request $request)
    {   
        Auth::guard('users')->invalidate($request->token);
        return ResponseFormatter::success(null, 'User has been logged out');
    }
 
    public function logoutCustomer(Request $request)
    {   
        Auth::guard('customers')->invalidate($request->token);
        return ResponseFormatter::success(null, 'User has been logged out');
    }
 
    public function userAdmin(Request $request)
    {
        $user = Auth::guard('users')->authenticate($request->token);
        return ResponseFormatter::success($user);
    }
 
    public function userCustomer(Request $request)
    {
        $user = Auth::guard('customers')->authenticate($request->token);
        return ResponseFormatter::success($user);
    }
}