<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;

class ApiControllerHelpers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->model = \App\Models\Product::class;
    }

    public function index(Request $request)
    {
        $where = [];
        if($request->all('where') && $request->all('where')['where'] != null){
            $columnWhere = $request->all('where')['where'];
            $columnWhere = explode(';', $request->all('where')['where']);
            foreach($columnWhere as $key => $value){
                $splitAgain = explode(':', $value);
                if($splitAgain[1]){
                    $where[] = [$splitAgain[0], '=', $splitAgain[1]];
                }
            }
        }
        $select = "*";
        if($request->all('select') && $request->all('select')['select'] != null){
            $columnSelect = $request->all('select')['select'];
            $select = explode(';', $request->all('select')['select']);
        }
        return ResponseFormatter::success($this->model::where($where)->select($select)->paginate(), 'Paginate Data');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modelFunction = new $this->model;
        $data = $request->only($modelFunction->fillableColumn());

        $validator = Validator::make($data, $modelFunction->validationColumn());

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        $storeData = $this->model::create($data);

        return ResponseFormatter::success($storeData, 'Created Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ResponseFormatter::success($this->model::find($id), 'Row Data');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelFunction = new $this->model;
        $primaryKeyName = $modelFunction->getKeyName();

        $user = $this->model::find($id);
        if(!$user) return ResponseFormatter::error(null, 'Not Found', 404);

        $data = $request->only($modelFunction->fillableColumn());

        $validator = Validator::make($data, $modelFunction->validationColumn());

        if ($validator->fails()) {
            return ResponseFormatter::error($validator->messages(), 'Validation Failed', 501);
        }

        $updateData = $this->model::where($primaryKeyName, $id)->update($data);

        return ResponseFormatter::success($data, 'Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->model::find($id);
        if(!$user) return ResponseFormatter::error(null, 'Not Found', 404);
        $user->delete();
        return ResponseFormatter::success(null, 'Delete Success');
    }
}