<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Http\Controllers\ApiControllerHelpers;
use Illuminate\Http\Request;

class TransactionController extends ApiControllerHelpers
{
    public function __construct()
    {
        $this->model = \App\Models\Transaction::class;
    }
}
