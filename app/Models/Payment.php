<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = 'mysql_transactions';
    use HasFactory;

    protected $fillable = [
        'payment_payload' 
    ];

    public function fillableColumn(){
        return 
        [
            'payment_payload'
        ];
    }

    public function validationColumn(){
        return 
        [
            'payment_payload' => 'required'
        ];
    }
}
