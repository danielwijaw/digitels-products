<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'products_name',
        'products_qty'    
    ];

    public function fillableColumn(){
        return 
        [
            'products_name',
            'products_qty'  
        ];
    }

    public function validationColumn(){
        return 
        [
            'products_name' => 'required|string',
            'products_qty' => 'required'
        ];
    }
}
