<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $connection = 'mysql_transactions';

    use HasFactory;

    protected $fillable = [
        'transactions_customers_id',
        'transactions_products_id',
        'transactions_status'
    ];

    public function fillableColumn(){
        return 
        [
            'transactions_customers_id',
            'transactions_products_id',
            'transactions_status'
        ];
    }

    public function validationColumn(){
        return 
        [
            'transactions_customers_id' => 'required',
            'transactions_products_id' => 'required'
        ];
    }
}
