<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_transactions')->create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('transactions_customers_id');
            $table->integer('transactions_products_id');
            $table->string('transactions_status')->default('waiting payment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_transactions')->dropIfExists('transactions');
    }
}
