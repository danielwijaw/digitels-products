<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);

Route::post('register-customer', [ApiController::class, 'registerCustomer']);
Route::post('login-customer', [ApiController::class, 'authenticateCustomer']);

Route::group(['middleware' => ['auth:customers']], function() {
    Route::get('logout-customer', [ApiController::class, 'logoutCustomer']);
    Route::get('get-customer', [ApiController::class, 'userCustomer']);
    Route::get('products-list', [ProductController::class, 'index']);
    Route::get('products-list/{id}', [ProductController::class, 'show']);
    Route::resource('transactions-list', 'App\Http\Controllers\TransactionController');
});

Route::group(['middleware' => ['auth:users']], function() {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get-user', [ApiController::class, 'userAdmin']);
    Route::resource('products', 'App\Http\Controllers\ProductController');
    Route::resource('transactions', 'App\Http\Controllers\TransactionController');
});

// Route::group(['middleware' => ['jwt.verify']], function() {
//     Route::get('logout', [ApiController::class, 'logout']);
//     Route::get('get_user', [ApiController::class, 'get_user']);
//     Route::get('products', [ProductController::class, 'index']);
//     Route::get('products/{id}', [ProductController::class, 'show']);
//     Route::post('create', [ProductController::class, 'store']);
//     Route::put('update/{product}',  [ProductController::class, 'update']);
//     Route::delete('delete/{product}',  [ProductController::class, 'destroy']);
// });
